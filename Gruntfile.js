module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-rename');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.initConfig({
        compile_dir: 'app',
        dist_dir: 'dist',
        src: {
            js: {
                app: ["app/src/panda/main.js", "app/src/panda/locale/pl.js", "app/src/panda/locale/en_us.js", "app/src/panda/model.js", "app/src/panda/collection.js", "app/src/panda/view.js", "app/src/panda/views/model.js", "app/src/panda/template_loader.js", ],
                lib: ['node_modules/jquery/dist/jquery.js', 'node_modules/underscore/underscore.js', 'node_modules/backbone/backbone.js', 'node_modules/knockout/dist/knockout.js', ]
            }
        },
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            js_lib: ['app/js/lib.js', 'app/js/lib.min.js', 'app/js/pandabackbone.js', 'app/js/pandabackbone.min.js'],
        },
        concat: {
            js_lib: {
                src: ['<%= src.js.lib %>'],
                dest: 'app/js/lib.js'
            },
            js_app: {
                src: ['<%= src.js.app %>'],
                dest: 'app/js/pandabackbone.js'
            }
        },
        copy: {
            pub_js: {
                expand: true,
                cwd: "app/js/",
                src: "pandabackbone*.js",
                dest: '<%= dist_dir %>'
            }
        },
        uglify: {
            js_app: {
                options: {
                    banner: '/* PandaBackbone v<%= pkg.version %> . */\n'
                },
                src: ['app/js/pandabackbone.js'],
                dest: 'app/js/pandabackbone.min.js'
            },
            js_lib: {
                src: ['app/js/lib.js'],
                dest: 'app/js/lib.min.js'
            }
        },
        karma: {
            unit: {
                singleRun: true,
                configFile: 'karma.conf.js'
            }
        },
        connect: {
            root: {
                options: {
                    base: 'app',
                    keepalive: true
                }
            }
        }
    });
    grunt.registerTask('init', ['clean', 'concat:js_lib', 'uglify:js_lib']);
    grunt.registerTask('build', ['concat:js_app', 'uglify:js_app']);
    grunt.registerTask('tests', ['concat:js_app', 'karma'])
    grunt.registerTask('publish', ['init', 'build', 'tests', 'copy:pub_js']);
};