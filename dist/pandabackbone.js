Pb = {
  initialize: function(config) {
    var _config = Backbone.Model.extend({
      defaults: {
        locale: 'pl',
        auto_load_relations: false
      }
    });
    this.config = new _config(config);
  },
  locales: {},
  objectNotation: function(key, initial) {
    if (initial === undefined) {
      initial = window;
    }
    return key.split('.').reduce(function(obj, name) {
      return obj[name];
    }, initial);
  },
  c: function(key, value) {
    if (value !== undefined) {
      this.config.set(key, value);
    }
    return this.config.get(key);
  },
  t: function(key, obj) {
    /*
     Funkcja tłumaczenia umożliwia podanie obiektu z wartościami które będa wpisane/wypełniona w tłumaczeniu. 'Witaj {{name}} w {{city.name}}'.
     Tłumaczenie użyte w takis posób
      Pb.t('locale.some.text',{
        name: 'Janek',
        city: {
          name: Poznaniu
        }
      });
      dla tekstu powyżej wyświetli
      Witaj Janek w Poznaniu
    */
    var text = this.objectNotation(key, Pb.locales[this.c('locale')]); // Pobiera text z danego locale.
    if (obj !== undefined) { // jeżeli są jakieś zmienne to wypełnienia
      text = text.replace(/\{\{([^\{]+)\}\}/g, function(match, name) {
        var names = name.split('.');
        if (obj[names[0]] != undefined) { // sprawdzam czy chociaż pierwszy klucz jest poprawny
          return this.objectNotation(name, obj);
        }
      }.bind(this));
    }
    return text;
  },
  uncamelize: function(text) {
    text = text.replace(/[A-Z]/g, function(letter) {
      return '_' + letter.toLowerCase();
    });
    return text.replace(/^_/, '');
  }
};
Pb.locales.pl = {
  locale_type: "Język polski",
  name: 'Poland',
  name_short: 'pl,PL',
  validators: {
    required: "To pole jest wymagane",
    email: {
      invalid: "Niepoprawny adres email."
    },
    string: {
      invalid: "To nie jest poprawny tekst.",
      too_short: 'Wpisany tekst jest zbyt krótki',
      too_long: 'Wpisany tekst jest zbyt długi',
      min_length: 'Minimalna długość wpisanego tekstu to {{len}} znaki/znaków.',
      max_length: 'Maksymalna długość wpisanego tekstu to {{len}} znaki/znaków.'
    },
    integer: {
      invalid: "To nie jest poprawna liczba"
    },
    bool: {
      invalid: "Niepoprawna wartość"
    }
  }
};
Pb.locales.en_us = {
  locale_type: "English",
  name: 'English, U.S.',
  name_short: 'en,EN,us,US',
  validators: {
    required: "Required field.",
    email: {
      invalid: "Invalid email format."
    },
    string: {
      invalid: "Invalid text.",
      too_short: 'Text is too short.',
      too_long: 'Text is too long.',
      min_length: 'Minimal length of text is {{len}}.',
      max_length: 'Maximum length of text is {{len}}.'
    },
    integer: {
      invalid: "Wrong number."
    },
    bool: {
      invalid: "Invalid value"
    }
  }
};
Pb.Model = Backbone.Model.extend({
  initialize: function() {
    this._errors = {}
    this._initRelations();
  },
  validate: function(keys) {
    /* Obsługuje tak naprawdę dwa sposoby walidacji
      / 1. Domyslny z Backbone'a odpalany pośrednio po model.isValid() i model.save(). Wtedy odpala się z pustym obiektem {}.
      / Wtedy ta funkcja umożliwia zwrócienie tablicy błędów w przypadku błędnej walidacji i nie zwrócienie niczego! w przypadku poprawnej walidacji. 
      / 2. Druga opcja to odpalanie beżpośrednio gdy chcemy zwalidować tylko konkretne pola np model.validate(['email','name','phone']) wtedy przepisuje je do domyślnej tablicy błędów i zwraca tę tablicę 
       W przypadku 2 jeżeli odpali się bez podania tablicy pól to zwaliduje wszystkie pola
    */
    var fields = this.fields;
    this._errors = {}; // usuwam poprzednio wygenerowane błedy na modelu
    this.validationError = null; // usuwam poprzednio wygenerowane błędy na modelu
    if (keys && _.isArray(keys)) { // jeżeli podano tablicę pól które mają być walidowane to pobierz je z this.fields{}. Tylko one będa walidowane. Odpalenie save(), isValid() itd. znowu będzie walidować domyślnie na wszystkich argumentach
      fields = {};
      _.each(keys, function(key) {
        if (this.fields[key] !== undefined) {
          fields[key] = this.fields[key];
        }
      }, this);
    }
    for (var key in fields) { // przeleć po wszystkich polach i wygeneruj błąd o polach wymaganych required = true bądź ustaw domyślne w zależności od typu jeżeli nie są wymagane
      var field = fields[key],
        value = this.attributes[key];
      if (field.validate === undefined || field.validate) { // domyślnie walidacja jest włączona. Trzeba ustawić validate = false wtedy w ogóle tu nie włazi i nie ustawia np. domyślnego pola
        if (!this.has(key)) { // jeżeli pole nie jest ruszone
          if (field.required) {
            this.addError(key, Pb.t('validators.required'));
          } else {
            this.setDefaultValue(field, key, value);
          }
        }
        value = this.attributes[key]; // zmienione wartości
        switch (field.type.toLowerCase()) {
          case 'email':
            if ((field.required || value.length) && !this._validateEmail(value)) {
              this.addError(key, Pb.t('validators.email.invalid'));
            }
            break;
          case 'boolean':
            if (value === 'on' || value === 1 || value === '1') {
              this.set(key, true);
            }
            if (value === 'off' || value === 0 || value === '0') {
              this.set(key, false);
            }
            if (!_.isBoolean(this.attributes[key])) {
              this.addError(key, Pb.t('validators.bool.invalid'));
            }
            break;
          case 'integer':
          case 'int':
          case 'number':
            if (!_.isNumber(value)) {
              this.addError(key, Pb.t('validators.integer.invalid'));
            }
            break;
          case 'string':
          case 'text':
            if (!_.isString(value)) {
              this.addError(key, Pb.t('validators.string.invalid'));
            } else {
              if ((field.min_length != null) && value.length < field.min_length) {
                this.addError(key, Pb.t('validators.string.min_length', {
                  len: field.min_length
                }));
              }
              if ((field.max_length != null) && value.length > field.max_length) {
                this.addError(key, Pb.t('validators.string.max_length', {
                  len: field.max_length
                }));
              }
            }
            break;
        }
      }
    }
    if (_.keys(this._errors).length) { // jeżeli są jakikolwiek błędy to zostają zwrócone w return. Jeżeli nie to nie zwróci nic. Taki pomysł z Backone'a.
      if (keys === undefined || _.isArray(keys)) { //jeżeli nie podano nic  co ma być walidowane jawnie lub podana konkretne wartości to przepisuje je do validationError bo to NIE jest domyślna walidacja Backbone'a tylko odpalona z ręki
        this.validationError = this._errors;
      }
      return this._errors;
    }
  },
  _initRelations: function() {
    var relation;
    for (var i in this.relations) {
      relation = this.relations[i];
      switch (relation.type.toLowerCase()) {
        case 'has_many':
        case 'hasmany':
        case 'hm':
        case 'h_m':
          func_name = "_" + Pb.uncamelize(i);
          this[func_name] = (function(_this, _relation) {
            return function(clb) {
              var collection, _collection = Pb.Collection.extend({
                model: Pb.objectNotation(_relation.model),
                url: _this.replaceUrl(_relation.url)
              });
              collection = new _collection();
              var auto_load_relations = _relation.auto_load_relations !== undefined ? _relation.auto_load_relations : Pb.c('auto_load_relations');
              if (clb === undefined && auto_load_relations === true) {
                collection.fetch();
              } else if (typeof clb == 'function') {
                collection.fetch({
                  success: function(coll, raw_data) {
                    clb(true, collection, raw_data);
                  },
                  error: function() {
                    clb(false, collection, []);
                  }
                });
              }
              return collection;
            };
          })(this, relation);
          break;
      }
    }
  },
  replaceUrl: function(url) {
    var value, regex;
    for (var key in this.attributes) {
      value = this.attributes[key];
      regex = new RegExp("/:" + key, "g");
      url = url.replace(regex, "/" + value);
    }
    return url;
  },
  setDefaultValue: function(field, key, value) {
    switch (field.type.toLowerCase()) {
      case 'email':
      case 'string':
      case 'text':
        if (typeof value !== 'string') {
          return this.set(key, '');
        }
        break;
      case 'boolean':
        if (value === 'on' || value === 1 || value === '1') {
          this.set(key, true);
        }
        if (value === 'off' || value === 0 || value === '0') {
          this.set(key, false);
        }
        if (typeof this.get(key) !== 'boolean') {
          return this.set(key, false);
        }
        break;
      case 'int':
      case 'integer':
      case 'number':
        var val = parseInt(value, 10);
        if (isNaN(val)) {
          return this.set(key, 0);
        } else {
          this.set(key, val);
        }
    }
  },
  addError: function(key, value) {
    if (this._errors[key] === undefined) {
      this._errors[key] = [];
    }
    this._errors[key].push(value);
  },
  _validateEmail: function(email) {
    var re;
    re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
});
Pb.Collection = Backbone.Collection.extend({
  
});
Pb.View = Pb.View || [];
Pb.View.Main = Backbone.View.extend({
  render: function(data) {
    this.$el.append(this.template(data));
    return this;
  },
  renderWithModel: function() {
    if (this.model && this.collection) {
      return this.render({
        model: this.model.toJSON(),
        collection: this.collection.toJSON()
      })
    } else if (this.model) {
      return this.render({
        model: this.model.toJSON()
      })
    } else if (this.collection) {
      return this.render({
        collection: this.collection.toJSON()
      })
    }
  },
  renderWithData: function(data) {
    if (data) {
      return this.render({
        collection: data
      })
    } else {
      this.render();
    }
  }
});
Pb.View.Model = Pb.View.Main.extend({
  initialize: function() {
    this.setDefaultValues();
    this.initEvents();
  },
  render: function() {
    Pb.View.Main.prototype.render.apply(this, arguments);
    ko.applyBindings(new this.ViewModel(this, this.model));
  },
  initEvents: function() {
    /*
    Inicjuje wszystkie eventy. Bedą one odpalone przez ko po zmianie pola. 
    Eventy mają nazwę _{atrybut}OnChange. Tego nie można przeciążac
    Przed opalaenium tych powyżej odpalana jest funkcja _{atrybut}OnBeforeChange. Jeżeli zwróci ona false wówczas funkcja wyżej nie odpali się.
    Po każdym odpaleniu _{atrybut}Onchange odpalana jest również walidacja modelu.
    Podobnie jak powyżej odpalana jest funckja onBeforeValidate. Jeżeli zwróci false to proces walidacji i wyświetlania errorów się zatrzyma.
    Po walidacji jest odpalana funkcja onAfterValidate.
    Dla wszystkich funkcji Onbefore... można | nie trzeba wstawić własną logikę
    */
    for (var key in this.model.attributes) {
      attribute = this.model.attributes[key];
      var func_name = "_" + key + "OnChange";
      this[func_name] = (function(_key, _this) {
        return function(new_value) {
          var before_function_name = _key + "OnBeforeChange";
          var proceed = true;
          if (typeof _this[before_function_name] === "function" && _this[before_function_name](new_value) === false) {
            proceed = false;
          }
          if (proceed === true) {
            _this.removeErrors();
            _this.model.set(_key, new_value);
            if (_this.onBeforeValidate(_key, new_value) !== false) {
              _this._onValidate(_key, new_value);
              _this.onAfterValidate(_key, new_value);
            }
          }
        }
      })(key, this);
    }
  },
  setDefaultValues: function() {
    /* 
    Przed render ko wiąże słuchacze do wszystkich pól. 
    Muszą one posiadać wartości domyślne niezależnie czy są wymagane czy nie. 
    Tutaj są one tworzone
    */
    _.each(this.model.fields, function(field, key) {
      this.model.setDefaultValue(field, key, this.model.get(key));
    },this);
  },
  onBeforeValidate: function(key, value) {},
  onAfterValidate: function(key, value) {},
  _onValidate: function(key, value) {
    if (!this.model.isValid()) {
      this.showErrors();
    }
  },
  uiInput: function(name) {
    return this.$("input[name=" + name + "]", "select[name=" + name + "]").first();
  },
  showErrors: function() {
    if (this.model && this.model.validationError) {
      _.each(this.model.validationError, function(val, key) {
        var input = this.uiInput(key);
        if (input) {
          input.parent("div").addClass("has-error");
          input.after("<div class='error'><span>" + val + "</span></div>");
        }
      }, this);
    }
  },
  removeErrors: function() {
    this.$('div.error').remove()
    this.$('.form-group.has-error').removeClass("has-error")
  },
  ViewModel: function(parentView, model) {
    var attribute;
    model.validate(); // ustawia wartości domyślne dla modelu
    for (var key in model.attributes) {
      attribute = model.attributes[key];
      this[key] = ko.observable(model.get(key));
      var func_name = "_" + key + "OnChange";
      this[key].subscribe(parentView[func_name]);
    }
  }
});

Pb.TemplateLoader = {
  load: function(views, callback, dir, view_namespace) {
    if (dir === undefined) {
      dir = "templates/";
    }
    if (view_namespace === undefined) {
      view_namespace = Pb.View;
    }
    var deferreds = [];
    $.each(views, function(index, view) {
      if (view_namespace[view]) {
        deferreds.push($.get(dir + view.toLowerCase() + '.tpl', function(data) {
          view_namespace[view].prototype.template = _.template(data);
        }, 'html'));
      } else {
        throw view + " nie istnieje";
      }
    });
    $.when.apply(null, deferreds).done(callback);
  }
};