Pb.TemplateLoader = {
  load: function(views, callback, dir, view_namespace) {
    if (dir === undefined) {
      dir = "templates/";
    }
    if (view_namespace === undefined) {
      view_namespace = Pb.View;
    }
    var deferreds = [];
    $.each(views, function(index, view) {
      if (view_namespace[view]) {
        deferreds.push($.get(dir + view.toLowerCase() + '.tpl', function(data) {
          view_namespace[view].prototype.template = _.template(data);
        }, 'html'));
      } else {
        throw view + " nie istnieje";
      }
    });
    $.when.apply(null, deferreds).done(callback);
  }
};