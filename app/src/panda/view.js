Pb.View = Pb.View || [];
Pb.View.Main = Backbone.View.extend({
  render: function(data) {
    this.$el.append(this.template(data));
    return this;
  },
  renderWithModel: function() {
    if (this.model && this.collection) {
      return this.render({
        model: this.model.toJSON(),
        collection: this.collection.toJSON()
      })
    } else if (this.model) {
      return this.render({
        model: this.model.toJSON()
      })
    } else if (this.collection) {
      return this.render({
        collection: this.collection.toJSON()
      })
    }
  },
  renderWithData: function(data) {
    if (data) {
      return this.render({
        collection: data
      })
    } else {
      this.render();
    }
  }
});