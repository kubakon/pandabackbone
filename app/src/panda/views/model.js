Pb.View.Model = Pb.View.Main.extend({
  initialize: function() {
    this.setDefaultValues();
    this.initEvents();
  },
  render: function() {
    Pb.View.Main.prototype.render.apply(this, arguments);
    ko.applyBindings(new this.ViewModel(this, this.model));
  },
  initEvents: function() {
    /*
    Inicjuje wszystkie eventy. Bedą one odpalone przez ko po zmianie pola. 
    Eventy mają nazwę _{atrybut}OnChange. Tego nie można przeciążac
    Przed opalaenium tych powyżej odpalana jest funkcja _{atrybut}OnBeforeChange. Jeżeli zwróci ona false wówczas funkcja wyżej nie odpali się.
    Po każdym odpaleniu _{atrybut}Onchange odpalana jest również walidacja modelu.
    Podobnie jak powyżej odpalana jest funckja onBeforeValidate. Jeżeli zwróci false to proces walidacji i wyświetlania errorów się zatrzyma.
    Po walidacji jest odpalana funkcja onAfterValidate.
    Dla wszystkich funkcji Onbefore... można | nie trzeba wstawić własną logikę
    */
    for (var key in this.model.attributes) {
      attribute = this.model.attributes[key];
      var func_name = "_" + key + "OnChange";
      this[func_name] = (function(_key, _this) {
        return function(new_value) {
          var before_function_name = _key + "OnBeforeChange";
          var proceed = true;
          if (typeof _this[before_function_name] === "function" && _this[before_function_name](new_value) === false) {
            proceed = false;
          }
          if (proceed === true) {
            _this.removeErrors();
            _this.model.set(_key, new_value);
            if (_this.onBeforeValidate(_key, new_value) !== false) {
              _this._onValidate(_key, new_value);
              _this.onAfterValidate(_key, new_value);
            }
          }
        }
      })(key, this);
    }
  },
  setDefaultValues: function() {
    /* 
    Przed render ko wiąże słuchacze do wszystkich pól. 
    Muszą one posiadać wartości domyślne niezależnie czy są wymagane czy nie. 
    Tutaj są one tworzone
    */
    _.each(this.model.fields, function(field, key) {
      this.model.setDefaultValue(field, key, this.model.get(key));
    },this);
  },
  onBeforeValidate: function(key, value) {},
  onAfterValidate: function(key, value) {},
  _onValidate: function(key, value) {
    if (!this.model.isValid()) {
      this.showErrors();
    }
  },
  uiInput: function(name) {
    return this.$("input[name=" + name + "]", "select[name=" + name + "]").first();
  },
  showErrors: function() {
    if (this.model && this.model.validationError) {
      _.each(this.model.validationError, function(val, key) {
        var input = this.uiInput(key);
        if (input) {
          input.parent("div").addClass("has-error");
          input.after("<div class='error'><span>" + val + "</span></div>");
        }
      }, this);
    }
  },
  removeErrors: function() {
    this.$('div.error').remove()
    this.$('.form-group.has-error').removeClass("has-error")
  },
  ViewModel: function(parentView, model) {
    var attribute;
    model.validate(); // ustawia wartości domyślne dla modelu
    for (var key in model.attributes) {
      attribute = model.attributes[key];
      this[key] = ko.observable(model.get(key));
      var func_name = "_" + key + "OnChange";
      this[key].subscribe(parentView[func_name]);
    }
  }
});
