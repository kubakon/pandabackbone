Pb = {
  initialize: function(config) {
    var _config = Backbone.Model.extend({
      defaults: {
        locale: 'pl',
        auto_load_relations: false
      }
    });
    this.config = new _config(config);
  },
  locales: {},
  objectNotation: function(key, initial) {
    if (initial === undefined) {
      initial = window;
    }
    return key.split('.').reduce(function(obj, name) {
      return obj[name];
    }, initial);
  },
  c: function(key, value) {
    if (value !== undefined) {
      this.config.set(key, value);
    }
    return this.config.get(key);
  },
  t: function(key, obj) {
    /*
     Funkcja tłumaczenia umożliwia podanie obiektu z wartościami które będa wpisane/wypełniona w tłumaczeniu. 'Witaj {{name}} w {{city.name}}'.
     Tłumaczenie użyte w takis posób
      Pb.t('locale.some.text',{
        name: 'Janek',
        city: {
          name: Poznaniu
        }
      });
      dla tekstu powyżej wyświetli
      Witaj Janek w Poznaniu
    */
    var text = this.objectNotation(key, Pb.locales[this.c('locale')]); // Pobiera text z danego locale.
    if (obj !== undefined) { // jeżeli są jakieś zmienne to wypełnienia
      text = text.replace(/\{\{([^\{]+)\}\}/g, function(match, name) {
        var names = name.split('.');
        if (obj[names[0]] != undefined) { // sprawdzam czy chociaż pierwszy klucz jest poprawny
          return this.objectNotation(name, obj);
        }
      }.bind(this));
    }
    return text;
  },
  uncamelize: function(text) {
    text = text.replace(/[A-Z]/g, function(letter) {
      return '_' + letter.toLowerCase();
    });
    return text.replace(/^_/, '');
  }
};