Pb.locales.pl = {
  locale_type: "Język polski",
  name: 'Poland',
  name_short: 'pl,PL',
  validators: {
    required: "To pole jest wymagane",
    email: {
      invalid: "Niepoprawny adres email."
    },
    string: {
      invalid: "To nie jest poprawny tekst.",
      too_short: 'Wpisany tekst jest zbyt krótki',
      too_long: 'Wpisany tekst jest zbyt długi',
      min_length: 'Minimalna długość wpisanego tekstu to {{len}} znaki/znaków.',
      max_length: 'Maksymalna długość wpisanego tekstu to {{len}} znaki/znaków.'
    },
    integer: {
      invalid: "To nie jest poprawna liczba"
    },
    bool: {
      invalid: "Niepoprawna wartość"
    }
  }
};