Pb.locales.en_us = {
  locale_type: "English",
  name: 'English, U.S.',
  name_short: 'en,EN,us,US',
  validators: {
    required: "Required field.",
    email: {
      invalid: "Invalid email format."
    },
    string: {
      invalid: "Invalid text.",
      too_short: 'Text is too short.',
      too_long: 'Text is too long.',
      min_length: 'Minimal length of text is {{len}}.',
      max_length: 'Maximum length of text is {{len}}.'
    },
    integer: {
      invalid: "Wrong number."
    },
    bool: {
      invalid: "Invalid value"
    }
  }
};