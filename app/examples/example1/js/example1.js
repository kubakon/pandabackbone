$(function() {
  App = {};
  Pb.TemplateLoader.load(["UserForm"], function() {
    Pb.initialize();
    var user = new Example1.User({
      email: 'asd',
      first_name: 'Jan ad aoduh adu ahd iaud haidu ahd aiud hadi uadh aiud ahd asd is'
        //last_name celowo nie podaję
    });
    var view = new Example1.View.UserForm({
      model: user
    });
    view.renderWithModel();
  }, '/examples/example1/templates/', Example1.View);
});
Example1 = {
  View: {}
};
//----------- Model ---------------------------
Example1.User = Pb.Model.extend({
  fields: {
    email: {
      type: 'email',
      required: true
    },
    first_name: {
      type: 'string',
      required: true,
      max_length: 15,
      min_length: 4
    },
    last_name: {
      type: 'string',
      required: true,
      min_length: 15
    }
  }
});
//---------------View --------------------------
Example1.View.UserForm = Pb.View.Main.extend({
  el: 'body',
  events: {
    "click button": "onSave"
  },
  onSave: function(e) {
    this.model.set('email', $("input[name=email]").val()); // dlatego lepiej używać widoku MVVM
    this.model.set('first_name', $("input[name=first_name]").val());
    this.model.set('last_name', $("input[name=last_name]").val());
    e.preventDefault();
    if (this.model.isValid()) {
      alert('Dane poprawne')
    } else {
      console.log("Niezbyt poprawne");
      $('code.errors').html(JSON.stringify(this.model.validationError));
    }
  }
});