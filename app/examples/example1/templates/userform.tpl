<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Model Przykład</div>
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" value="<%= model.email %>">
                            </div>
                            <div class="form-group">
                                <label>FirstName</label>
                                <input name="first_name" class="form-control" value="<%= model.first_name %>">
                            </div>
                            <div class="form-group">
                                <label>LastName</label>
                                <input name="last_name" class="form-control" value="<%= model.last_name %>">
                            </div>
                            <button  class="btn btn-default">Zapisz</button>
                        </form>
                        <p>Tablica błędów</p>
                        <pre><code class="errors">
                            
                        </code></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>