$(function() {
  App = {};
  Pb.TemplateLoader.load(["TestModelView"], function() {
    Pb.initialize();
    var test = new Example3.Test({
      email: 'jan@kowalski@mail.com',
      name: 'Jan Kowalski'
    });
    var view = new Example3.View.TestModelView({
      model: test
    });
    view.render();
  }, '/examples/example3/templates/', Example3.View);
});
Example3 = {
  View: {}
};
//----------- Model ---------------------------
Example3.Test = Pb.Model.extend({
  fields: {
    email: {
      type: 'email',
      required: true
    },
    name: {
      type: 'string',
      required: true,
      min_length: 15
    },
    checked: {
      type: 'boolean',
      required: true
    }
  }
});
//---------------View --------------------------
Example3.View.TestModelView = Pb.View.Model.extend({
  el: 'body',
  events: {
    "click button": "onSave"
  },
  onSave: function(e){
    e.preventDefault();
    if(this.model.isValid()){
      alert('Dane poprawne. Zapisuję :)')
    }
    else{
      this.showErrors();
    }
  },
  nameOnBeforeChange: function(new_value){
    console.log("Funkcja odpalona przed ustawieniem wartości name w modelu");
  },
  checkedOnBeforeChange: function(new_value){
    console.log("Funkcja odpalona przed ustawieniem wartości checked w modelu",new_value);
  },
  onAfterValidate: function(){
    console.log(this.model.validationError);
    console.log("Funkcja odpalona po walidacji modelu");
  }
});