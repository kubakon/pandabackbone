<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">MVVM Przykład</div>
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" data-bind="value: email">
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" class="form-control" data-bind="value: name">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="checked" data-bind="checked: checked" type="checkbox"> Checked?
                                </label>
                            </div>
                            <button  class="btn btn-default">Zapisz</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>