describe("Ustawianie domyślnych wartości w modelu.", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: true
        },
        second_name: {
          type: 'string'
        },
        email: {
          type: 'email',
          required: true
        },
        second_email: {
          type: 'email',
          required: false
        },
        third_email: {
          type: 'email',
          required: false
        },
        numb: {
          type: 'integer',
          required: true
        },
        second_number: {
          type: 'integer',
          required: true
        },
        last_number: {
          type: 'integer'
        },
        second_name: {
          type: 'string'
        },
        male: {
          type: 'boolean',
          required: true
        },
        women: {
          type: 'boolean'
        }
      }
    });
    model = new _model();
  });
  describe("W przypadku wpisania błędnych wartości domyślne wartości powinny być poprawne", function() {
     // Zmiana to działa tylko dla wartości nie ruszonych!!!!. Więc jeżeli wpiszemy błędną wartość to zwróci błąd i nie zmieni wartośc pola!!!!
  });
  describe("W przypadku nie wpisania niczego wartości nie wymagane!!!!! otrzymają wartości domyślne", function() {
    it("Dla stringów", function() {
      model.isValid();
      expect(model.get('second_name')).toEqual('');
    });
    it("Dla liczb", function() {
      model.validate();
      expect(model.get('last_number')).toEqual(0);
    });
    it("Dla mailii", function() {
      model.save();
      expect(model.get('second_email')).toEqual('');
    });
    it("Dla boolowskich", function() {
      model.save();
      expect(model.get('women')).toEqual(false);
    });
  });
});