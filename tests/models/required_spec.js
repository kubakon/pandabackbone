describe("Walidacja pól typu required modelu.", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email',
          required: true
        },
        second_email: {
          type: 'email',
          required: false
        },
        third_email: {
          type: 'email',
          required: false
        },
        numb: {
          type: 'integer',
          required: true
        },
        second_number: {
          type: 'integer',
          required: true
        },
        second_name: {
          type: 'string'
        },
        male: {
          type: 'boolean',
          required: true
        },
        women: {
          type: 'boolean'
        }
      }
    });
    model = new _model();
  });
  it("Zwróci błedy wszystkich pól które mają ustawionione wartość required", function() {
    model.save();
    expect(model.validationError).toEqual(jasmine.any(Object));
    expect(model.validationError.name[0]).toEqual(jasmine.any(String));
    expect(model.validationError.email[0]).toEqual(jasmine.any(String));
    expect(model.validationError.numb[0]).toEqual(jasmine.any(String));
    expect(model.validationError.numb[0]).toEqual(jasmine.any(String));
    expect(model.validationError.second_number[0]).toEqual(jasmine.any(String));
    expect(model.validationError.male[0]).toEqual(jasmine.any(String));
  });
  it("Nie zwróci błedów dla pozostałych", function() {
    var errors = model.validate();
    expect(errors).toEqual(jasmine.any(Object));
    expect(errors.second_email).not.toBeDefined();
    expect(errors.third_email).not.toBeDefined();
    expect(errors.second_name).not.toBeDefined();
    expect(errors.women).not.toBeDefined();
  });
  it("Dla pól typu email zwróci błąd mimo że wpiszemy tam wartość '' jeżeli pole jest wymagane", function() {
    model.set('email',"");
    model.validate();
    expect(model.validationError.email).toBeDefined();
    expect(model.validationError.email[0]).toEqual(jasmine.any(String));
  });
  it("Dla pól typu email nie zwróci żadnego błędu jeżeli pole nie jest wymagane jeżeli nie podamy żadnej wartości lub wpiszemy ''", function() {
    model.set('second_email','');
    model.validate();
    expect(model.validationError.second_email).not.toBeDefined();
  });
});