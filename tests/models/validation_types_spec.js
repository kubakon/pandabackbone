describe("Walidacja konkretnych typów modelu", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: true
        },
        second_name: {
          type: 'string'
        },
        third_name: {
          type: 'string',
          min_length: 5
        },
        text: {
          type: 'string',
          max_length: 5
        },
        email: {
          type: 'email',
          required: true
        },
        second_email: {
          type: 'email',
          required: false
        },
        third_email: {
          type: 'email',
          required: false
        },
        numb: {
          type: 'integer',
          required: true
        },
        second_number: {
          type: 'number',
          required: true
        },
        male: {
          type: 'boolean',
          required: true
        },
        women: {
          type: 'boolean'
        }
      }
    });
    model = new _model();
  });
  describe("Typu String i text", function() {
    it("Wpisując złe wartości klasa zwróci odpowiedni komunikat niezależnie czy pole jest wymagane czy nie", function() {
      model.set('name', 123);
      model.set('second_name', []);
      model.set('third_name', ['asd', 123]);
      model.set('text', false);
      model.save();
      var errors = model.validationError;
      expect(errors).toBeDefined();
      expect(errors.name[0]).toEqual(jasmine.any(String));
      expect(errors.second_name[0]).toEqual(jasmine.any(String));
      expect(errors.third_name[0]).toEqual(jasmine.any(String));
      expect(errors.text[0]).toEqual(jasmine.any(String));
    });
    it("Jeżeli pole posiada atrybut min_length. Powinno zwrócic odpowiedni komunkat", function() {
      model.set('third_name', 'asd');
      var errors = model.validate();
      expect(errors).toBeDefined();
      expect(errors.third_name[0]).toEqual(jasmine.any(String));
    });
    it("Jeżeli pole posiada atrybut max_length. Powinno zwrócic odpowiedni komunkat", function() {
      model.set('text', 'asd aksjdh sad haskdaj hdkadjhsadk jahdksa ds');
      var errors = model.validate();
      expect(errors).toBeDefined();
      expect(errors.text[0]).toEqual(jasmine.any(String));
    });
    it("Jeżeli pole posiada atrybut min_length, a nie posiada opcji required. Powinno zwrócic odpowiedni komunkat", function() {
      model.validate();
      expect(model.validationError.third_name[0]).toEqual(jasmine.any(String));
    });
  });
  describe("Typu Email", function() {
    it("Wpisują inne typy niż string klasa wpisze zawsze pusty ciąg znaków w dane pole", function() {});
    describe("Wpisują złe wartości typu String nie będące poprawnym adresem email, klasa doda odpowiedni error", function() {
      it("1.", function() {
        model.set('email',"zły adres@email.com");
        model.set('third_email',"@email.com");
        expect(model.isValid()).toBeFalsy();
        var error = model.validationError;
        expect(error.email[0]).toEqual(jasmine.any(String));
        expect(error.third_email[0]).toEqual(jasmine.any(String));
      });
      it("2.", function() {
        model.set('email',"janek.kowalski@email.com123.123");
        model.set('third_email',"123@email.com12");
        expect(model.isValid()).toBeFalsy();
        var error = model.validationError;
        expect(error.email[0]).toEqual(jasmine.any(String));
        expect(error.third_email[0]).toEqual(jasmine.any(String));
      });
    });
  });
  describe("Typu Number, Integer, Int", function() {
    it("Podanie typu 'int', 'integer','number' powinno być równoznaczne", function() {
      model.set('numb', false);
      model.set('second_number', false);
      model.validate();
      expect(model.validationError.numb).toEqual(model.validationError.second_number);
      model.set('numb', true);
      model.set('second_number', true);
      model.validate();
      expect(model.validationError.numb).toEqual(model.validationError.second_number);
      model.set('numb', [123]);
      model.set('second_number', [123]);
      model.validate();
      expect(model.validationError.numb).toEqual(model.validationError.second_number);
      model.set('numb', {});
      model.set('second_number', {});
      model.validate();
      expect(model.validationError.numb).toEqual(model.validationError.second_number);
    });
    it("Wpisują złe wartości klasa wpisze zawsze 0 w wartość pola", function() { // to już nie działa !!!!
    });
  });
  describe("Typu Boolean", function() {
    it("Wpisują złe wartości klasa wpisze zawsze false w wartość pola", function() {}); // to już nie działa
    it("Dla 'on' true ", function() {
      model.set('women', "on");
      model.validate();
      expect(model.get('women')).toBeTruthy();
    });
    it("Dla '1' true ", function() {
      model.set('male', '1');
      model.validate();
      expect(model.get('male')).toBeTruthy();
    });
    it("Dla 1 true ", function() {
      model.set('male', 1);
      model.validate();
      expect(model.get('male')).toBeTruthy();
    });
    it("Dla 'off' false ", function() {
      model.set('women', 'off');
      model.validate();
      expect(model.get('women')).toBeFalsy();
    });
    it("Dla '0' false ", function() {
      model.set('women', "0");
      model.validate();
      expect(model.get('women')).toBeFalsy();
    });
    it("Dla 0 false ", function() {
      model.set('women', 0);
      model.validate();
      expect(model.get('women')).toBeFalsy();
    });
  });
});