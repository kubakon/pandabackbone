describe("Walidacja modelu dla pól z opcją validate", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: false,
          validate: true
        },
        email: {
          type: 'email',
          required: true,
          validate: false
        },
        numb: {
          type: 'integer',
          validate: true
        },
        second_number: {
          type: 'integer',
          required: true
        },
        third_number: {
          type: 'integer',
          required: false
        },
        male: {
          type: 'boolean'
        },
        text: {
          type: 'text',
          required: true,
          validate: false
        }
      }
    });
    model = new _model();
  });
  it("Zwróci odpowiednie błędy dla pól z opcja validate = true bądź nie ustawioną w ogóle", function() {
    model.set('name', 999);
    model.set('numb', []);
    model.set('second_number', [123]);
    model.set('third_number', {
      a: 155
    });
    model.set('male', 'tak');
    model.isValid();
    var errors = model.validationError;
    expect(errors).toBeDefined();
    expect(errors.name[0]).toEqual(jasmine.any(String));
    expect(errors.numb[0]).toEqual(jasmine.any(String));
    expect(errors.second_number[0]).toEqual(jasmine.any(String));
    expect(errors.third_number[0]).toEqual(jasmine.any(String));
    expect(errors.male[0]).toEqual(jasmine.any(String));
  });
  it("Nie zwróci odpowiednich błędów dla pól z opcja validate = false, niezależnie od wpisanych wartość", function() {
    model.set('name', '999');
    model.set('numb', 123);
    model.set('second_number', [123]);
    model.set('text',123);
    model.isValid();
    var errors = model.validationError;
    expect(errors).toBeDefined();
    expect(errors.text).not.toBeDefined();
    expect(errors.email).not.toBeDefined();
    expect(errors.name).not.toBeDefined();
    expect(errors.numb).not.toBeDefined();
    expect(errors.second_number[0]).toEqual(jasmine.any(String));
    model.set('email', 'zły mail');
    model.save();
    expect(model.validationError.email).not.toBeDefined();
  });
});