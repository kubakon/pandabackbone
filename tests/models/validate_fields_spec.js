describe("Walidacja modelu odpalana bezpośrednio", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email',
          required: true
        },
        second_email: {
          type: 'email',
          required: false
        },
        third_email: {
          type: 'email',
          required: false
        },
        numb: { // nie można wpisywać pola number !!
          type: 'integer',
          required: true
        },
        second_number: {
          type: 'integer',
          required: true
        },
        second_name: {
          type: 'string'
        },
        male: {
          type: 'boolean',
          required: true
        },
        women: {
          type: 'boolean'
        }
      }
    });
    model = new _model();
  });
  it("Gdy nie poda się żadnych pól to zwaliduje wszystkie pola modelu", function() {
    var errors = model.validate();
    expect(errors.name[0]).toEqual(jasmine.any(String));
    expect(errors.email[0]).toEqual(jasmine.any(String));
    expect(errors.numb[0]).toEqual(jasmine.any(String));
    expect(errors.second_number[0]).toEqual(jasmine.any(String));
    expect(errors.male[0]).toEqual(jasmine.any(String));
  });
  describe("Gdy nie poda się odpowidnie pola zwróci komunikaty błędów tylko dla nich, a w validationError będzię się znajdować dokładnie ten sam obiekt błędów", function() {
    it("Dla pustych pól", function() {
      var errors = model.validate(['email', 'name', 'women', 'second_name']);
      expect(errors.name[0]).toEqual(jasmine.any(String));
      expect(errors.email[0]).toEqual(jasmine.any(String));
      expect(errors.numb).not.toBeDefined();
      expect(errors.second_number).not.toBeDefined();
      expect(errors.male).not.toBeDefined();
      expect(errors.women).not.toBeDefined();
      expect(errors.second_email).not.toBeDefined();
      expect(errors.third_email).not.toBeDefined();
      expect(errors.second_name).not.toBeDefined();
      expect(errors).toEqual(model.validationError);
    });
    it("Dla wypełnionych/ustawionych pól", function() {
      model.set('email', 'chuasd @ wp 123');
      model.set('numb', '666');
      model.set('second_number', false);
      model.set('women', 999);
      var errors = model.validate(['email','second_number','male','third_email','second_name']);
      expect(errors.name).not.toBeDefined();
      expect(errors.email[0]).toEqual(jasmine.any(String));
      expect(errors.numb).not.toBeDefined();
      expect(errors.second_number[0]).toEqual(jasmine.any(String));
      expect(errors.male[0]).toEqual(jasmine.any(String));
      expect(errors.women).not.toBeDefined();
      expect(errors.second_email).not.toBeDefined();
      expect(errors.third_email).not.toBeDefined();
      expect(errors.second_name).not.toBeDefined();
      expect(errors).toEqual(model.validationError);
    });
  });
});