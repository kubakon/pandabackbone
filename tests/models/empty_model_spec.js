describe("Walidacja pól modelu.", function() {
  var model;
  beforeEach(function() {
    Pb.initialize();
    var _model = Pb.Model.extend({
      fields: {
        name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email',
          required: true
        },
        not_required: {
          type: 'string'
        }
      }
    });
    model = new _model();
  });
  describe("Pustego tzn takiego w którym nie podało się żadnych wartości.", function() {
    it("Zwróci obiekt błedów dla każdego z pól wymaganych", function() {
      var errors = model.validate();
      expect(errors).toBeDefined();
      expect(errors.name).toBeDefined();
      expect(errors.name).toEqual(jasmine.any(Array));
      expect(errors.name[0]).toEqual(jasmine.any(String));
      expect(errors.email).toBeDefined();
      expect(errors.email).toEqual(jasmine.any(Array))
      expect(errors.email[0]).toEqual(jasmine.any(String))
    });
    it("a isValid() zwróci false", function() {
      expect(model.isValid()).toBeFalsy();
    });
    it("po odpaleniu isValid() w zmiennej validationError będą się znajdować komunikaty błędów", function() {
      model.isValid();
      expect(model.validationError).toBeDefined();
      expect(model.validationError).toEqual(jasmine.any(Object));
      expect(model.validationError.name[0]).toEqual(jasmine.any(String));
    });
    it("save() zwróci false", function() {
      expect(model.save()).toBeFalsy();
    });
    it("po odpaleniu save() w zmiennej validationError będą się znajdować komunikaty błędów", function() {
      model.save();
      expect(model.validationError).toBeDefined();
      expect(model.validationError).toEqual(jasmine.any(Object));
      expect(model.validationError.name[0]).toEqual(jasmine.any(String));
    });
    it("Nie zwróci błędów dla pól które nie mają opcji 'required'", function() {
      var errors = model.validate();
      expect(errors).toBeDefined();
      expect(errors.not_required).not.toBeDefined();
    });
  });
  describe("Po wstawieniu części wartości", function() {
    it("Zwróci obiekt błedów dla tych które zawierają błędy", function() {
      model.set('name', 'Janek');
      var errors = model.validate();
      expect(errors).toBeDefined();
      expect(errors.email).toBeDefined();
      expect(errors.email).toEqual(jasmine.any(Array))
      expect(errors.email[0]).toEqual(jasmine.any(String))
    });
    it("a isValid() zwróci false", function() {
      expect(model.isValid()).toBeFalsy();
    });
    it("po odpaleniu isValid() w zmiennej validationError będą się znajdować komunikaty błędów", function() {
      model.set('email', 'jan@nowak.pl');
      model.isValid();
      expect(model.validationError.name[0]).toEqual(jasmine.any(String));
    });
    it("save() zwróci false", function() {
      expect(model.save()).toBeFalsy();
    });
    it("po odpaleniu save() w zmiennej validationError będą się znajdować komunikaty błędów", function() {
      model.set('email', 'jan@nowak.pl');
      model.save();
      expect(model.validationError.name[0]).toEqual(jasmine.any(String));
    });
    it("Nie zwróci błędów dla pól które zostały ustawione'", function() {
      model.set('email', 'jan@nowak.pl');
      model.set('name', 'janek');
      expect(model.isValid()).toBeTruthy();
      expect(model.validationError).toBeNull();
    });
  });
});