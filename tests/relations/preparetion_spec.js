describe("Relacje modelu has_many", function() {
  App = {};
  var student;
  beforeEach(function(done) {
    Pb.initialize();
    var students;
    App.student = Pb.Model.extend({
      fields: {
        first_name: {
          type: 'string',
          required: true
        },
        last_name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email'
        }
      },
      relations: {
        grades: {
          url: "/base/tests/_data/students/:id/grades.json",
          type: 'has_many',
          model: 'App.student_grade'
        },
        no_existing_relation: {
          url: "/base/tests/_data/students/:email/elems",
          type: 'has_many',
          model: 'App.non_existing_elems_only_for_testing_urls'
        },
        non_existing_relation: {
          url: "/base/tests/_data/students/:id/with_some_name/:first_name/:last_name/something/in/something",
          type: 'has_many',
          model: 'App.non_existing_relation_model'
        },
        VeryStupidRelationName: {
          url: "/base/tests/_data/students/:id/with_some_name/:id/:last_name/:id/something/in/something",
          type: 'has_many',
          model: 'App.non_existing_relation_model'
        }
      }
    });
    App.student_grade = Pb.Model.extend({
      name: {
        type: 'string',
        required: true
      },
      value: {
        type: 'integer',
        required: true
      },
      subject_id: {
        type: 'integer',
        required: true
      },
      user_id: {
        type: 'integer',
        required: true
      }
    });
    App.collection = Pb.Collection.extend({
      url: "/base/tests/_data/students.json",
      model: App.student
    })
    students = new App.collection();
    students.fetch({
      success: function() {
        student = students.get(1)
        done()
      }
    })
  });
  it("Instancja modelu powinna zawierać wszystkie metody relacji w postani _relationName() utworzone z modelu konfiguracyjnego relacji ", function() {
    expect(student['_grades']).toBeDefined();
    expect(student['_no_existing_relation']).toBeDefined();
    expect(student['_non_existing_relation']).toBeDefined();
    expect(student['_grades']).toEqual(jasmine.any(Function));
    expect(student['_no_existing_relation']).toEqual(jasmine.any(Function));
    expect(student['_non_existing_relation']).toEqual(jasmine.any(Function));
    expect(student['_very_stupid_relation_name']).toEqual(jasmine.any(Function));
  });
  it("Dla każdych z tych relacji powinien wygenerować odpowiednie url w postaci np. /students/:id/books -> /students/12/books itd. ", function(done) {
    student._grades(function(success, collection) {
      expect(collection.url).toEqual("/base/tests/_data/students/1/grades.json");
    });
    student._no_existing_relation(function(success, collection) {
      expect(collection.url).toEqual("/base/tests/_data/students/jan.kowalski@email.com/elems");
    });
    student._non_existing_relation(function(success, collection) {
      expect(collection.url).toEqual("/base/tests/_data/students/1/with_some_name/Jan/Kowalski/something/in/something");
    });
    student._very_stupid_relation_name(function(success, collection) {
      expect(collection.url).toEqual("/base/tests/_data/students/1/with_some_name/1/Kowalski/1/something/in/something");
      done();
    });
  });
});