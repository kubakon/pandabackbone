describe("Pobieranie kolekcji elementów.", function() {
  var students;
  beforeEach(function() {
    Pb.initialize();
    _model = Pb.Model.extend({
      fields: {
        first_name: {
          type: 'string',
          required: true
        },
        last_name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email'
        }
      }
    });
    var _collection = Pb.Collection.extend({
      url: "/base/tests/_data/students.json",
      model: _model
    })
    students = new _collection();
  });
  it("fetch() odpalone na collekcji powinno załadować tablicę modeli", function(done) {
    students.fetch({
      success: function(data){
        expect(students.length).toBe(3);
        done();
      }
    });
  });
});