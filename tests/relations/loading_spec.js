describe("Relacje modelu has_many", function() {
  App = {};
  var student;
  beforeEach(function(done) {
    Pb.initialize();
    var students;
    App.student = Pb.Model.extend({
      fields: {
        first_name: {
          type: 'string',
          required: true
        },
        last_name: {
          type: 'string',
          required: true
        },
        email: {
          type: 'email'
        }
      },
      relations: {
        grades: {
          url: "/base/tests/_data/students/:id/grades.json",
          type: 'has_many',
          model: 'App.student_grade'
        },
        auto_loaded_grades: {
          auto_load_relations: true,
          url: "/base/tests/_data/students/:id/grades.json",
          type: 'has_many',
          model: 'App.student_grade'
        }
      }
    });
    App.student_grade = Pb.Model.extend({
      name: {
        type: 'string',
        required: true
      },
      value: {
        type: 'integer',
        required: true
      },
      subject_id: {
        type: 'integer',
        required: true
      },
      user_id: {
        type: 'integer',
        required: true
      }
    });
    App.collection = Pb.Collection.extend({
      url: "/base/tests/_data/students.json",
      model: App.student
    })
    students = new App.collection();
    students.fetch({
      success: function() {
        student = students.get(1)
        done();
      }
    })
  });
  describe("Każda z metod powinna mieć możliwość dostać parametr w postaci funkcji zwrotnej w której będzie się znajdować załadowana kolekcja odpowiednich modeli", function() {
    it("gdy poda się callback", function(done) {
      student._grades(function(success, collection, raw_data) {
        expect(success).toBeTruthy();
        expect(raw_data).toEqual(jasmine.any(Array));
        expect(raw_data.length).toEqual(3);
        expect(raw_data.length).toEqual(collection.length);
        var grade = collection.at(0);
        expect(grade.get('name')).toEqual(jasmine.any(String));
        expect(grade.get('name').length).toBeGreaterThan(3);
        done();
      });
    });
    it("gdy  się nie poda się callback i opcja `auto_load_relations` jest ustawiona na true to funkcja zwróci załadowaną collekcję. Oczywiśce w trybue asynchronicznym", function(done) {
      var collection = student._auto_loaded_grades();
      setTimeout(function() {
        expect(collection.toJSON()).toEqual(jasmine.any(Array));
        expect(collection.length).toEqual(3);
        var grade = collection.at(0);
        expect(grade.get('value')).toEqual(jasmine.any(Number));
        expect(grade.get('value')).toBeGreaterThan(0);
        done();
      }, 1000);
    });
    it("Funkcja zwraca kolekcję można ją załadować ręcznie jak każdą inna kolekcję w momencie w którym się chce", function(done) {
      var collection = student._grades();
      collection.fetch({
        success: function(grades, raw_data) {
          expect(raw_data).toEqual(jasmine.any(Array));
          expect(raw_data.length).toEqual(3);
          expect(raw_data.length).toEqual(collection.length);
          var grade = collection.at(0);
          expect(grade.get('user_id')).toEqual(student.get('id'));
          done();
        }
      });
    });
  });
});