describe("Tłumaczenie tekstu dla innego języka", function() {
  beforeEach(function() {
    Pb.initialize({
      locale: 'en_us'
    });
  });
  it("Powinno zwrócić poprawny tekst dla obiektu nie zagnieżdżonego", function() {
    expect(Pb.t('locale_type')).toBeDefined();
    expect(Pb.t('locale_type')).toEqual(Pb.locales.en_us.locale_type);
  });
  it("Powinno zwrócić poprawną zagnieżdżonegą wartość", function() {
    expect(Pb.t('validators.string.invalid')).toBeDefined();
    expect(Pb.t('validators.string.invalid')).toEqual(Pb.locales.en_us.validators.string.invalid);
  });
});