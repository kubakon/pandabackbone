describe("Tłumaczenie tekstu z wypełnianiem zmiennych", function() {
  beforeEach(function() {
    Pb.initialize({
      locale: 'my_own'
    });
    Pb.locales.my_own = {
      simple_text: 'Works',
      replace_simple_text: 'Hello {{name}}',
      replace_simple_name: 'Hello {{name}}. Nice to meet you {{name}} from {{city}}.',
      nested: {
        obj: {
          in_other: {
            nested: {
              obj: {
                text: "Some text {{values.value1}} and {{values.value1}} and {{values.nested.values.value2}}."
              }
            }
          }
        }
      }
    };
  });
  it("Prosty tekst", function() {
    expect(Pb.t('simple_text')).toBeDefined();
    expect(Pb.t('simple_text')).toEqual(jasmine.any(String));
    expect(Pb.t('simple_text')).toEqual('Works');
  });
  it("Powinno zamienić tekst ze zmiennymi", function() {
    expect(Pb.t('replace_simple_text', {
      name: 'Panda'
    })).toEqual('Hello Panda');
  });
  it("Powinno  globalnie zamienić tekst ze zmiennymi", function() {
    expect(Pb.t('replace_simple_name', {
      name: 'Jacek',
      city: 'Poznań'
    })).toEqual('Hello Jacek. Nice to meet you Jacek from Poznań.');
  });
  it("Powinno  globalnie zamienić tekst ze zmiennymi zagnieżdżonymi i tekstem też zagnieżdżonym", function() {
    var value1 = "Some stupid text",
      value2 = "Some another stupid text";
    expect(Pb.t('nested.obj.in_other.nested.obj.text', {
      values: {
        value1: value1,
        nested: {
          values: {
            value2: value2
          }
        }
      }
    })).toEqual("Some text " + value1 + " and " + value1 + " and " + value2 + ".");
  });
});