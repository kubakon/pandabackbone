describe("Tłumaczenie tekstu", function() {
  beforeEach(function() {
    Pb.initialize();
  });
  describe("Domyślne czyli pl", function() {
    it("Powinno zwrócić poprawny tekst dla obiektu nie zagnieżdżonego", function() {
      expect(Pb.t('locale_type')).toBeDefined();
      expect(Pb.t('locale_type')).toEqual(Pb.locales.pl.locale_type);
    });
    it("Powinno zwrócić poprawną zagnieżdżonegą wartość", function() {
      expect(Pb.t('validators.string.invalid')).toBeDefined();
      expect(Pb.t('validators.string.invalid')).toEqual(Pb.locales.pl.validators.string.invalid);
    });
  });
});